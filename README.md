# Hand Pose Estimation from Single Depth image
CS5340 Project
Group 6: Vu Dinh Quang Dat, Won Jun Ru Daphne

This is our implement in python 3 for the project.

The project contains:
- README.md: this instruction
- final-report.pdf: final report pdf file
- src/: code folder
- Evaluation_Dataset/: dataset folder. Can be download from [here](http://hpes.bii.a-star.edu.sg/index.php/instructions)

## Requirements
Our code requires scipy, numpy, scikit-learn, skimage packages in python enviroment.
- For Windows 64bit (7, 8, 10), we recommended to use Anaconda with Python 3.5 ([download from here](https://www.continuum.io/downloads), all required packages are preinstalled in Anaconda). Make sure you set Anaconda as the default python environment.
- For Linux and OSX, you can also use [Anaconda](https://www.continuum.io/downloads) or install all scipy packages manually.

## Running the program
The program can be ran using command line.
Create output for test data:
```bash
$ cd src
$ python main.py test
```

Run cross validation
```
$ cd src
$ python main.py cross-validation
```