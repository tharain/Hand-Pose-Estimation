# train.py
from sklearn.ensemble import RandomForestRegressor
import numpy as np
from plot_data import show_image
from skimage.feature import hog

def make_depth_features(depth, OFFSET_U, OFFSET_V):
	height = depth.shape[0]
	width = depth.shape[1]
	R = np.zeros(width*height)
	getd = lambda x, y: depth.item((y,x)) if x < width and y < height and y >= 0 and x >= 0 else 32001
	for y in range(0, height):
		for x in range(0, width):
			d = depth.item((y,x))
			rv = getd(x + int(OFFSET_U/d), y) - getd(x, y + int(OFFSET_V/d))
			R[y*width+x] = rv / 32001
	# print("maxR = ", np.max(R), " and ", np.min(R))
	# print(R[40:200][40:280])
	return R

def make_hog_features(image):	
	return hog(image)

def make_features(depth, confi):
	height = depth.shape[0]
	width = depth.shape[1]	
	for y in range(0, height):
		for x in range(0, width):
			# if the depth is larger than a certain threshold, set it to 32001
			if depth[y][x] > 750:
				depth[y][x] = 32001
	F = make_hog_features(depth)
	F = np.append(F, make_depth_features(depth, -6000, -6000))
	return F

def train_data(train_pose_data):
	# Fix random seed
	np.random.seed(5555)
	# Use Random Forest regressor with 10 trees
	rf = RandomForestRegressor(n_estimators=10)
	X = []
	y = []
	qq = 0
	print("Creating features matrix...")
	for pose_data in train_pose_data:		
		depth = pose_data.read_depth_file()
		confi = pose_data.read_confi_file()
		hand = pose_data.read_hand_file()
		X.append(make_features(depth, confi))
		y.append(np.reshape(hand, 60))
		qq += 1
	print("Training data...")
	X = np.array(X)
	y = np.array(y)
	rf.fit(X, y)
	return rf
	