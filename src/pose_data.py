# pose_data.py

import csv
import os.path
import numpy as np
from app_conf import APP_CONF

IMG_SIZE = APP_CONF.width*APP_CONF.height

class PoseData:
	"""Pose data file information"""
	def __init__(self, data_path, row):
		self.data_path = data_path
		self.subject = row[0]
		self.pose = row[1]
		self.confi = row[2]
		self.depth = row[3]
		self.hand = row[4] if len(row) > 4 else ""

	# convert to string
	def __str__(self):
		return "{0},{1},{2},{3}".format(self.subject, self.pose, self.confi, self.depth)

	# read depth file data and convert to numpy matrix
	def read_depth_file(self):
		path = os.path.join(self.data_path, self.subject, self.depth)
		data = np.fromfile(path, dtype='short', count=IMG_SIZE)
		return np.reshape(data, (APP_CONF.height, APP_CONF.width))

	# read confidence map file data and convert to numpy matrix
	def read_confi_file(self):
		path = os.path.join(self.data_path, self.subject, self.confi)
		data = np.fromfile(path, dtype='short', count=IMG_SIZE)
		return np.reshape(data, (APP_CONF.height, APP_CONF.width))

	# read hand training data and convert to numpy matrix
	def read_hand_file(self):
		path = os.path.join(self.data_path, self.subject, self.hand)		
		data = np.loadtxt(path, dtype='float', delimiter=",")
		return np.reshape(data, (20, 3))


def read_pose_mappings(pose_mappings_file, data_path):
	poses_data = []
	# print(pose_mappings_file)
	with open(pose_mappings_file) as csvfile:
		reader = csv.reader(csvfile)
		for row in reader:
			poses_data.append(PoseData(data_path, row))
	return poses_data	