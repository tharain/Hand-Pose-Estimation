# app_conf.py

import os.path

def constant(f):
    def fset(self, value):
        raise TypeError
    def fget(self):
        return f()
    return property(fget, fset)

class _AppConf(object):
	eval_dataset_path = ""

	@constant
	def yourMethodName():
		return "myMethod"
	@constant
	def width():
		return 320
	@constant
	def height():
		return 240
	@constant
	def path_config_file():
		return "path.txt"	
	def test_path(self):
		return os.path.join(self.eval_dataset_path, "Test_Dataset")
	def train_path(self):
		return os.path.join(self.eval_dataset_path, "Training_Dataset")
	def pose_mappings_path(self):
		return os.path.join(self.eval_dataset_path, "subject_pose_mappings.csv")
	def pose_training_path(self):
		return os.path.join("../", "training_pose_mappings.csv")

APP_CONF = _AppConf()
