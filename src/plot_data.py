import matplotlib.pyplot as plt
import numpy as np

# Show depth and confidence map image
def show_image(depth, confi, hand_joints = []):
	plt.figure(".::Image::.")
	plt.imshow(confi, cmap=plt.get_cmap('gray'))
	plt.figure(".::Depth Image::.")
	plt.imshow(np.minimum(depth, 1024), cmap=plt.get_cmap('gray'))
	plt.show()