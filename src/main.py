# main.py

from pose_data import PoseData, read_pose_mappings
from app_conf import APP_CONF
from train import train_data, make_features
from plot_data import show_image
import numpy as np
import datetime
import os.path
import sys

# Fix Python 2.x.
try:
    import __builtin__
    input = getattr(__builtin__, 'raw_input')
except (ImportError, AttributeError):
    pass

# Write to file
def writeToCSVFile(results):
	# unique file name
	currentDate = datetime.date.today()
	resultFileName = "result_{:%m_%d_%Y}.csv".format(currentDate)
	if(os.path.exists(resultFileName) == False):
		tf = open(resultFileName, "a+").close()
	# write to file
	with open(resultFileName, "w") as file:
		for result in results:
			line = ','.join(['%.5f' % num for num in result]) + "\n"
			file.write(line)
		file.close()

# read path setting
def read_settings():
	pathFileName = APP_CONF.path_config_file

	if(os.path.exists(pathFileName) == False):
		tf = open(pathFileName, "a+")		
		item = input("What is the Evaluation_Dataset path ?")
		item = item + "\n"
		tf.write(item)
		tf.close()

	with open(pathFileName, "r") as pathFile:
		APP_CONF.eval_dataset_path = pathFile.readline().strip() # set 1st line as the path for Evaluation_Dataset
		pathFile.close()

if __name__ == "__main__":
	# read test_state
	test_state = sys.argv[1] if len(sys.argv) > 1 else 'test'

	# need to set path
	read_settings()

	# read train data
	print("Preparing data...")
	train_pose_data = read_pose_mappings(APP_CONF.pose_training_path(), APP_CONF.train_path())			

	# Fix random seed
	np.random.seed(5555)
	if test_state == 'cross-validation':
		# Split 50% of training data for evaluating
		train_size = int(len(train_pose_data)*0.6)
		test_pose_data = train_pose_data[train_size:]
		train_pose_data = train_pose_data[:train_size]
	elif test_state == 'test':
		# Read the csv file
		test_pose_data = read_pose_mappings(APP_CONF.pose_mappings_path(), APP_CONF.test_path())	

	# train the model
	train_model = train_data(train_pose_data)

	# go through each depth image
	result = []
	train_result = []
	qq = 0
	print("Testing data...")
	for pose_data in test_pose_data:	
		depth = pose_data.read_depth_file()
		confi = pose_data.read_confi_file()
		""" 
		#Understanding
		SUM of depth[i] = width = 240 = #no. columns
		SUM of depth[i][j] = height = 320 = #no. rows
		"""
		# for i in range(0,1):
		# 	print len(depth)		
		
		# show image, only use for debugging		
		# show_image(depth, confi)

		# your method here with (depth, confi, joints)
		X = np.reshape(make_features(depth, confi), (1, -1))
		y = train_model.predict(X)
		# print(y[0])

		#
		# joints = np.zeros(60)
		joints = y[0]		
		result.append(joints)
		if test_state == 'cross-validation':
			train_result.append(np.reshape(pose_data.read_hand_file(), 60))

		# break to view only first data set
		qq += 1
		if qq % 5 == 0:
			print("Completed " + str(qq) + " tests")

	if test_state == 'cross-validation':
		result_error = abs(np.subtract(result, train_result))
		error_avg = np.average(result_error)
		error_max = np.max(result_error)
		error_min = np.min(result_error)
		error_mean = np.mean(result_error)
		print("Cross validation result:")
		print("Average error: %.5f" % error_avg)
		print("Mean error: %.5f" % error_mean)
		print("Maximum error: %.5f" % error_max)
		print("Minimum error: %.5f" % error_min)
	elif test_state == 'test':
		# write result output
		writeToCSVFile(result)
