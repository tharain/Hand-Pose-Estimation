import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import sys
import csv

if __name__ == "__main__":
	points = input("Enter point list: ")
	Q = [float(x) for x in points.split(',')]
	print(Q)
	# Q = [0,0,0,50,-48,-46,75,-45,-38,86,-39,-29,99,1,-37,139,-8,-46,155,-19,-50,167,-25,-53,96,1,-16,132,-26,-20,129,-41,-20,124,-40,-21,95,1,9,119,-34,7,105,-46,8,93,-40,9,87,1,26,106,-26,27,95,-35,27,84,-29,27]

	fig = plt.figure()
	ax = fig.add_subplot(111, projection='3d')
	n = 100
	for i in range(1, 20):
		if i == 1 or i == 4 or i == 8 or i == 12 or i == 16:
			ax.plot([Q[0], Q[i*3]], [Q[1], Q[i*3+1]], [Q[2], Q[i*3+2]], c='r')
		else:
			ax.plot([Q[i*3-3], Q[i*3]], [Q[i*3-2], Q[i*3+1]], [Q[i*3-1], Q[i*3+2]], c='r')

	ax.set_xlabel('X Label')
	ax.set_ylabel('Y Label')
	ax.set_zlabel('Z Label')

	plt.show()